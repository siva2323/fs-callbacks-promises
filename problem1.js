const fs = require('fs');

function problem1(numberOfFilesNeeded)
{
    const promise = createDirectory()
    promise.then((result)=>{
        return createRandomJsonFiles(numberOfFilesNeeded);
    }).then((result)=>{
        return deleteFile(numberOfFilesNeeded);
    }).then((result)=>{
    }).catch((err)=>{
        throw (err)
    })
}
    function createDirectory(){
        return new Promise((resolve,reject)=>{
            fs.mkdir("../dummy", {recursive:true},err=>{
                if(err) 
                {
                    reject("Error occured "+err);
                }
                else resolve("directory successfully created");
            })
        })
    }

    function createRandomJsonFiles(numberOfFilesNeeded){
        return new Promise((resolve,reject)=>{
            for(let index=0;index<numberOfFilesNeeded;index++){
                fs.writeFile("../dummy/JSON"+index+".json", "message inside", (err) => {
                    if (err) 
                    {
                        reject(err);
                    }
                    else resolve("file successfully created");
                })
            }
        })
    }

    function deleteFile(numberOfFilesNeeded){
        return new Promise((resolve,reject)=>{
            for(let index=0; index<numberOfFilesNeeded;index++){
                fs.unlink("../dummy/JSON"+index+".json", (err)=>{
                    if(err) 
                    {
                        reject(err);
                    }
                    else resolve("file successfully deleted");
                })
            }
        })
    }


module.exports = problem1;