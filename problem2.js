const fs = require('fs');
const path=require("path");



function problem2(filePath)
{
    const promise = readFunction("lipsum.txt",filePath);
    promise.then((data)=>{
        return upperCaseFunction(data,filePath);

    }).then((result)=>{
        return addingFileNamesFunction("upperCase.txt",filePath);

    }).then((result)=>{
        return readFunction("upperCase.txt",filePath);

    }).then((data)=>{
        return lowerCaseFunction(data,filePath);

    }).then((result)=>{
        return addingFileNamesFunction("\nlowerCase.txt",filePath);

    }).then((result)=>{
        return readFunction("lowerCase.txt",filePath);

    }).then((data)=>{
        return sortingFunction(data,filePath);

    }).then((result)=>{
        return addingFileNamesFunction("\nsorted.txt",filePath);

    }).then((result)=>{
        return readFunction("fileNames.txt",filePath);

    }).then((data)=>{
        return deleteFunction(data,filePath);

    }).then((result)=>{

    }).catch((err)=>{
        console.error(err);
    })
}

    function readFunction(file,filePath){
        return new Promise((resolve,reject)=>{
            fs.readFile(filePath+file, 'utf-8',(err,data) =>{
                if(err) reject(err);
                else resolve(data);
            })
        })
    }

    function upperCaseFunction(data,filePath){                                                  
        return new Promise((resolve,reject)=>{
            fs.writeFile(filePath+"upperCase.txt",data.toUpperCase(), err =>{
                if(err) reject(err);
                else resolve("created upperCase.txt ");
            })
        })
    }

    function addingFileNamesFunction(fileName,filePath){
        return new Promise((resolve,reject)=>{
            fs.appendFile(filePath+"fileNames.txt", fileName, err=>{
                if(err) reject(err);
                else resolve("file name added");
            })
        })
    }

    function lowerCaseFunction(data,filePath){
        return new Promise((resolve,reject)=>{
            fs.writeFile(filePath+"lowerCase.txt",data.toLowerCase().replaceAll(".",".\n"),err=>{
                if(err) reject(err);
                else resolve("lowerCase.txt written");
            })
        })
    }

    function sortingFunction(data,filePath){
        return new Promise((resolve,reject)=>{
            fs.writeFile(filePath+"sorted.txt",data.split("\n").sort().join("\n"),err=>{
                if(err) reject(err);
                else resolve("successfully written sorted.txt");
            })
        })
    }

    function deleteFunction(data,filePath){
        return new Promise((resolve,reject)=>{
            let deleteList = data.split("\n");
            for(let index in deleteList){
                fs.unlink(filePath+deleteList[index],err=>{
                    if(err) reject(err);
                    else resolve(deleteList+" are deleted");
                })
            }
        })
    }


module.exports = problem2;

